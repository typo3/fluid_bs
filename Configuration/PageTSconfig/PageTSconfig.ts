mod.wizards.newContentElement {
    wizardItems {
        special.elements {
            fluid_bs_element {
                icon = icon goes here
                title = fluid_bs Content Element
                description = More info goes here
                tt_content_defValues {
                    CType = fluid_bs_fluid_bs
                }
            }
        }
        special.show := addToList(fluid_bs_element)
    }
}