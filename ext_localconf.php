<?php
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'UNIVERSITY_OF_ZURICH.' . $_EXTKEY,
    'Fluid_bs',
    array('Fluid_bs' => 'show'),
    array('Fluid_bs' => 'show'), // put here as well, if controller output must not be cached
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_bs/Configuration/PageTSconfig/PageTSconfig.ts">');

// By default, the 'tt_content' Header will be rendered: Avoid this,cause it's much nicer to use the header in the backend as a description title of what the record does.
$addLine = '
tt_content.fluid_bs_fluid_bs = COA
tt_content.fluid_bs_fluid_bs {
        10 >
}
';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript($_EXTKEY, 'setup', '# Setting ' . $_EXTKEY . $addLine . '', 'defaultContentRendering');
