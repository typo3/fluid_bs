#!/bin/bash

EXTENSION_KEY=fluid_bs
PLUGIN_NAME=fluid_bs
AUTHOR_NAME="Benjamin Baer"
AUTHOR_EMAIL="benjamin.baer@math.uzh.ch"
ORGANIZATION="University of Zurich"

#-------------------------------------------------------
# all configuration done - do not change something below
#-------------------------------------------------------


# Make all lowercase and remove unwanted chars
EXTENSION_KEY="${EXTENSION_KEY,,}"
EXTENSION_KEY=`echo "${EXTENSION_KEY}" | tr -c '[a-zA-Z0-9\n]' '_'`

# Make leading camelhook
EXTENSION_KEY_CAMEL="${EXTENSION_KEY^}"

# Make all lowercase and remove unwanted chars
PLUGIN_NAME="${PLUGIN_NAME,,}"
PLUGIN_NAME=`echo "${PLUGIN_NAME}" | tr -c '[a-zA-Z0-9\n]' '_'`

# Make leading camelhook
PLUGIN_NAME_CAMEL="${PLUGIN_NAME^}"

# Sanatize Author
AUTHOR_NAME=`echo "${AUTHOR_NAME}" | tr -c '[a-zA-Z0-9\.\-\n ]' '_'`
AUTHOR_EMAIL=`echo "${AUTHOR_EMAIL}" | tr -c '[a-zA-Z0-9@\.\-_\n]' '_'`
AUTHOR_FULL="${AUTHOR_NAME} <${AUTHOR_EMAIL}>"

# Sanatize Organization
ORGANIZATION=`echo "${ORGANIZATION}" | tr -c '[a-zA-Z0-9\n]' '_'`
ORGANIZATION="${ORGANIZATION^^}"

#--------------------------------------------------
function doResources() {
    mv Resources/Private/Templates/Sample Resources/Private/Templates/${EXTENSION_KEY_CAMEL}
}

#--------------------------------------------------
function doController() {
    mv Classes/Controller/SampleController.php  Classes/Controller/${EXTENSION_KEY_CAMEL}Controller.php
}

function replaceAll() {
    ALL=`find . -name "*.php" -type f ; find . -name "*.ts" -type f; find . -name "*.html" -type f; find . -name "Makefile" -type f; find . -name ".gitignore" -type f`
    for FILE in $ALL ; do
        sed -i "s/AUTHOR_FULL/${AUTHOR_FULL}/g" $FILE
        sed -i "s/AUTHOR_NAME/${AUTHOR_NAME}/g" $FILE
        sed -i "s/ORGANIZATION/${ORGANIZATION}/g" $FILE
        sed -i "s/EXTENSION_KEY_CAMEL/${EXTENSION_KEY_CAMEL}/g"  $FILE
        sed -i "s/EXTENSION_KEY/${EXTENSION_KEY}/g"  $FILE
        sed -i "s/PLUGIN_NAME_CAMEL/${PLUGIN_NAME_CAMEL}/g" $FILE
        sed -i "s/PLUGIN_NAME/${PLUGIN_NAME}/g" $FILE
    done
}

function renameCwd() {

    if [ -e ../$EXTENSION_KEY ] ; then
        echo "Directory not renamed: another file already exist"
        exit -1
    fi

    OLD_DIR=`pwd`
    cd ..
    mv $OLD_DIR $EXTENSION_KEY
    cd $EXTENSION_KEY
}

function removeGit() {
    chmod -R u+rwx .git
    rm -R .git
}

#============
#--- Main ---
replaceAll
doController
doResources

# Remove old 'git' config
removeGit

# At the end: rename own directory
renameCwd

make archive

cat <<EOF

GIT: The .git directory has been removed. Start from scratch.
Typo3: Import this extension now by uploading and installing ${EXTENSION_KEY}.zip

CWD: `pwd`
EOF


