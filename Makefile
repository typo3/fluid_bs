
all: archive

archive: clean fluid_bs.zip

fluid_bs.zip: Classes Configuration Resources ext_emconf.php ext_localconf.php ext_tables.php
	zip -r $@ $^

clean:
	rm -f fluid_bs.zip
