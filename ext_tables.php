<?php
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'UNIVERSITY_OF_ZURICH.' . $_EXTKEY,
    'Fluid_bs',
    'Fluid_bs Element',
    '[none]'
);

/*
 * The type name is generated by combining the extension key and plugin name, all lowercase and concatenated with `_'
 */

$GLOBALS['TCA']['tt_content']['types']['fluid_bs_fluid_bs'] = array(
    'showitem' => 'CType, header, bodytext'
);



