<?php
/**
 * @author Benjamin Baer <benjamin.baer@math.uzh.ch>
 */
$EM_CONF[$_EXTKEY] = array(
    'title' => 'fluid_bs Extension',
    'description' => 'fluid_bs Extension',
    'category' => 'Frontend',
    'author' => 'Benjamin Baer',
    'dependencies' => 'fluid,extbase',
    'clearcacheonload' => true,
    'state' => 'alpha',
    'version' => '0.1.0'
);