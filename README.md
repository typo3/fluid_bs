Typo3 Extbase Extension Rundown
===============================

* This template is configured to be used as a template for frontend extensions, used to render tt-content records.
* To create a new extension based on this template:

Start:

    git clone git@git.math.uzh.ch:typo3/typo3_sample_extension.git
    cd typo3_sample_extension
     
    # customize configure.sh by providing extension_key, plugin_name, author name, author email address.
    # The following script will rename the current directory to the extension_key name.
    ./configure.sh
    
    Go to the "Typo3 Backend > Extension manager", upload/install the extension zip file, 
    clear the Typo3 cache, create a new tt-content record of type 'extension key'.
    In the frontend you should see a uniq id - a new one on every page load.
    
    Change `/<extensionkey>/Classes/Controller/<Name>Controller.php` to your needs.
    

General
=======

For the simplest possible extbase extension, following directories and files are required:
 
    /<extensionkey>/
    /<extensionkey>/Classes/Controller/<Name>Controller.php
    /<extensionkey>/Configuration/PageTSconfig/PageTSconfig.ts
    /<extensionkey>/Resources/Private/Template/<Name>/<Action>.html
    /<extensionkey>/ext_emconf.php
    /<extensionkey>/ext_localconf.php
    /<extensionkey>/ext_tables.php

 * `<extensionkey>` is the extension key all lower-case.
 * `<Name>` is the name of a controller, and is also used in `ext_localconf.php`.
 * `<Action>` is the name of a controller  action, e.g. `show` or `list`

   

ext_tables.php
--------------

In `ext_tables.php`, we make the plugin available to the backend by calling

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        /* The extension name in UpperCamelCase */
        'Imath.' . $_EXTKEY,
        /* Plugin name in UpperCamelCase. Must be unique */
        'SamplePluginName',
        /* Plugin title. Used in backend drop down menu */
        'Sample Element',
        '<Path to Icon>'
    );

After calling this method, the plugin is made availabe in the backend. However, it has no functionality, yet.

The items shown in the backend, can are configured by using

    $GLOBALS['TCA']['tt_content']['types']['sampleextension_samplepluginname'] = array(
        'showitem' => 'CType, header, bodytext'
    );

The last key is derived from the extension key and plugin name by lowercase concatenation. In the example above, the 
extension key is `sampleextension` and the plugin name `SamplePluginName` (the same name used when calling 
`registerPlugin()`).
   
   
ext_localconf.php
-----------------

In `ext_localconf.php`, we configure the plugin behavior by calling
 
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        /* The extension name in UpperCamelCase */
        'Imath.' . $_EXTKEY,
        /* Plugin name in UpperCamelCase. Must be unique */
        'SamplePluginName',
        /* Controller Actions */
        array('Sample' => 'show'),
        /* Non-Cacheable Controller Actions */
        array('Sample' => 'show'),
        /* Plugin Type */
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
    );

The first two arguments of `configurePlugin()` are identical to the call of `registerPlugin()` in `ext_tables.php`.

The Controller Actions are an assoc array, where the key refers to the name of the controller. When looking for the 
controller class, extbase adds `Controller` to the key and looks for a class in `Classes/Controller/`. In the example
above, `Sample` becomes `SampleController`. Further, the class must be defined in a namespace 
`Imath\$_EXTKEY\Controller` (the namespace is derived from the extension name).

The value of the key are the actions exposed by the controller. Typo3 finds the method to call on the controller 
class by adding `Action` to the action, e.g., `show` becomes `showAction`.

If it is not desired to have the output of the controller cached, add the controller action in the same fashion to an 
assoc array and pass that as third argument to `configurePlugin()`.


Templates & Controllers
-----------------------

Templates should be living under 
`Resources/Private/Templates`. By convention, the template for a given controller action is located by looking at the
name of the controller class and stripping off `Controller`. For instance, if the controller is named 
`SampleController`, Typo3 looks for a directory `Sample` beneath `Resources/Private/Templates`. It then looks at the 
name of the controller action (method) and strips off `Action`, thus, in our example, `showAction()` becomes `show` 
and Typo3 tries to locate the template file `Show.html` in the `Sample` directory.
  
The controller action can create variables, that are available in the corresponding template, by calling 
`$this->view->assign('<variableName>', <value>)`.
 
All controllers should be put into `Classes/Controller`.  The controller in the sample is defined as follows

    namespace Imath\Sampleextension\Controller;
    
    class SampleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
        public function showAction() {
            $this->view->assign('sampleOutput', '<strong>' . uniqid() . '</strong>');
            return $this->view->render();
        }
    }

When we put the content element on a page, Typo3 knows because of the call to `configurePlugin()` in `ext_localconf
.php`, that the `SampleController` is in charge of the content element.